#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <stdalign.h>
#include <stddef.h>
#include <complex.h>
#include <math.h>
#include <errno.h>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <png.h>

#define CACHE_ALIGN 64
#define ALIGN alignas(CACHE_ALIGN)
#define DELTA_COR 0.004f
#define GL(line) do { \
    line; \
    if (glGetError() != GL_NO_ERROR) jkl(__LINE__); \
} while (0)

typedef unsigned char uchar;
typedef unsigned int uint;

typedef union {
    alignas(16) struct { float r, g, b, a; };
    alignas(16) float c[4];
} Color;

SDL_Window *window = NULL;
SDL_GLContext context;
SDL_Event event;
int scrnW = 800;
int scrnH = 600;

void jkl (int i);
GLuint get_shader (GLenum type, const char *path);
int write_pngfile (const char *filename, GLuint fbo);
int read_pngfile (const char *filename, GLuint fbo);

enum flags { QUIT = 0x01, FPS = 0x02, VSYNC = 0x04, ERASE = 0x08, CHANGE_RED = 0x10, CHANGE_GREEN = 0x20, CHANGE_BLUE = 0x40, CHANGE_ALPHA = 0x80, ANTI_ALIAS = 0x0100 };

int main (int argc, char **argv) {
    ALIGN uint16_t flag = VSYNC | ANTI_ALIAS;
    GLuint vao, vbo, fbo, tex, prog, vertsh, fragsh;
    GLenum drawbuf, err;
    GLint pos_attr, cor_uni, status;
    uint32_t t, prev_ft, dt, fps = 0, prev_st;
    const uint8_t *keystat = SDL_GetKeyboardState(NULL);
    const char *shpath[] = { "pincel.v.glsl", "pincel.f.glsl" };
    ALIGN float complex pos[8] = {}; // center
    Color cor = { 0.1f, 0.0f, 1.0f, 1.0f }; // blue
    Color delta = { 0.0f, 0.0f, 0.0f, 0.0f }; // change in color
    Color eraser = { 0.0f, 0.0f, 0.0f, 0.0f }; // tranparent
    struct { float w, h; } screen = { (float)scrnW, (float)scrnH };
    struct { float ersr, brsh, max; } size = { 5.0f, 2.0f, 7.0f };
    uint ndraws = 0; // number of lines or single point to draw this frame

    // initialization
    if (SDL_Init(SDL_INIT_VIDEO) < 0) jkl(-1);
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE) < 0) jkl(-2);
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2) < 0) jkl(-3);
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1) < 0) jkl(-4);
    //if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8) < 0) jkl(-5);
    if (!(window = SDL_CreateWindow("Pincel", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, scrnW, scrnH, SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI/* | SDL_WINDOW_FULLSCREEN_DESKTOP*/))) jkl(-6);
    SDL_GetWindowSize(window, &scrnW, &scrnH);
    if (!(context = SDL_GL_CreateContext(window))) jkl(-7);
    if (SDL_GL_SetSwapInterval(0) < 0) jkl(-8); // no vsync (vsync bugs the screen)
    glewExperimental = GL_TRUE;
    if ((err = glewInit()) != GLEW_OK) jkl(err);
    glViewport(0, 0, (GLsizei)scrnW, (GLsizei)scrnH);
    
    // setting up buffer objects
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pos), pos, GL_DYNAMIC_DRAW);

    // setting up framebuffer and texture
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, scrnW, scrnH, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glGenFramebuffersEXT(1, &fbo);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, tex, 0);
    drawbuf = GL_COLOR_ATTACHMENT0_EXT;
    glDrawBuffers(1, &drawbuf);
    glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);
    if (glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT) != GL_FRAMEBUFFER_COMPLETE_EXT) jkl(-8);

    // setting up shaders and their program
    vertsh = get_shader(GL_VERTEX_SHADER, shpath[0]);
    fragsh = get_shader(GL_FRAGMENT_SHADER, shpath[1]);
    prog = glCreateProgram();
    glAttachShader(prog, vertsh);
    glAttachShader(prog, fragsh);
    glLinkProgram(prog);
    glDetachShader(prog, vertsh);
    glDetachShader(prog, fragsh);
    glDeleteShader(vertsh);
    glDeleteShader(fragsh);
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status != GL_TRUE) jkl(-9);
    glUseProgram(prog);

    // attributes and uniforms uniforms
    if ((pos_attr = glGetAttribLocation(prog, "pos")) < 0) jkl(-20);
    glEnableVertexAttribArray(pos_attr);
    glVertexAttribPointer(pos_attr, 2, GL_FLOAT, GL_FALSE, sizeof(float complex), (void *)0);
    if ((cor_uni = glGetUniformLocation(prog, "cor")) < 0) jkl(-21);
    glUniform4fv(cor_uni, 1, cor.c);

    // screen
    glEnable(GL_PROGRAM_POINT_SIZE);
    glDisable(GL_POLYGON_SMOOTH);
    glDisable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glPointSize(2.0f);
    glLineWidth(2.0f);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glPixelTransferf(GL_ALPHA_BIAS, 0.1f);
    //glPixelTransferf(GL_ALPHA_SCALE, 1.1f);
    glClear(GL_COLOR_BUFFER_BIT);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    prev_st = t = SDL_GetTicks();
    if (SDL_ShowCursor(SDL_ENABLE) < 0) jkl(22);
    SDL_GL_SwapWindow(window);

    { // use in values of struct size for portability
        GLint n;
        float ia, ib;
        float qr[2], rs[2], st[2];
        glGetFloatv(GL_POINT_SIZE_RANGE, qr);
        glGetFloatv(GL_POINT_SIZE_GRANULARITY, &ia);
        glGetFloatv(GL_ALIASED_LINE_WIDTH_RANGE, rs);
        glGetFloatv(GL_SMOOTH_LINE_WIDTH_RANGE, st);
        glGetFloatv(GL_SMOOTH_LINE_WIDTH_GRANULARITY, &ib);
        printf("POINT[%g:%g:%g]\nLINE[%g:%g:%g]\nALIASED_LINE:[%g:%g]\n", qr[0], ia, qr[1], st[0], ib, st[1], st[0], st[1]);
        glGetIntegerv(GL_MAX_DRAW_BUFFERS, &n);
        printf("MAX DRAW BUFFERS: %d\n", n);
    }

    for (int i = 1; i < argc; ++i) {
        if (!read_pngfile(argv[i], fbo))
            fprintf(stderr, "Could not read %s\n", argv[1]);
        else
            printf("Read %s\n", argv[i]);
    }
    
    while (~flag & QUIT) {
        prev_ft = t;
        t = SDL_GetTicks();
        dt = t - prev_ft;
        ndraws = 0;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    flag |= QUIT; break;
                case SDL_KEYDOWN:
                    if (event.key.repeat) break;
                    switch (event.key.keysym.sym) {
                        case SDLK_ESCAPE:
                            flag |= QUIT; break;
                        case SDLK_p:
                            flag ^= FPS;
                            printf("show FPS %s\n", flag & FPS ? "ON" : "OFF"); break;
                        case SDLK_c:
                            glClear(GL_COLOR_BUFFER_BIT);
                            glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
                            glClear(GL_COLOR_BUFFER_BIT);
                            SDL_GL_SwapWindow(window);
                            printf("Screen cleared\n"); break;
                        case SDLK_v:
                            flag ^= VSYNC;
                            printf("vsync %s\n", flag & VSYNC ? "ON" : "OFF"); break;
                        case SDLK_b:
                            flag ^= ERASE;
                            if (flag & ERASE) {
                                glUniform4fv(cor_uni, 1, eraser.c);
                                glDisable(GL_BLEND);
                                glPointSize(size.ersr);
                                glLineWidth(size.ersr);
                                printf("Using eraser\n");
                            } else {
                                glUniform4fv(cor_uni, 1, cor.c);
                                glEnable(GL_BLEND);
                                glPointSize(size.brsh);
                                glLineWidth(size.brsh);
                                printf("Using brush\n");
                            } break;
                        case SDLK_q:
                            keystat[SDL_SCANCODE_SPACE] ? (cor.r = 1.0f) : (delta.r = DELTA_COR);
							flag |= CHANGE_RED; break;
                        case SDLK_a:
                            keystat[SDL_SCANCODE_SPACE] ? (cor.r = 0.0f) : (delta.r = -DELTA_COR);
							flag |= CHANGE_RED; break;
                        case SDLK_w:
                            keystat[SDL_SCANCODE_SPACE] ? (cor.g = 1.0f) : (delta.g = DELTA_COR);
							flag |= CHANGE_GREEN; break;
                        case SDLK_s:
                            keystat[SDL_SCANCODE_SPACE] ? (cor.g = 0.0f) : (delta.g = -DELTA_COR);
							flag |= CHANGE_GREEN; break;
                        case SDLK_e:
                            keystat[SDL_SCANCODE_SPACE] ? (cor.b = 1.0f) : (delta.b = DELTA_COR);
							flag |= CHANGE_BLUE; break;
                        case SDLK_d:
                            keystat[SDL_SCANCODE_SPACE] ? (cor.b = 0.0f) : (delta.b = -DELTA_COR);
							flag |= CHANGE_BLUE; break;
                        case SDLK_r:
                            keystat[SDL_SCANCODE_SPACE] ? (cor.a = 1.0f) : (delta.a = DELTA_COR);
                            flag |= CHANGE_ALPHA; break;
                        case SDLK_f:
                            keystat[SDL_SCANCODE_SPACE] ? (cor.a = 0.0f) : (delta.a = -DELTA_COR);
                            flag |= CHANGE_ALPHA; break;
                        case SDLK_UP:
                            if (flag & ERASE) {
                                size.ersr = fminf(size.ersr + 1.0f, size.max);
                                glPointSize(size.ersr);
                                glLineWidth(size.ersr);
                                printf("eraser size: %2.0f\n", size.ersr);
                            } else {
                                size.brsh = fminf(size.brsh + 1.0f, size.max);
                                glPointSize(size.brsh);
                                glLineWidth(size.brsh);
                                printf("brush size: %2.0f\n", size.brsh);
                            } break;
                        case SDLK_DOWN:
                            if (flag & ERASE) {
                                size.ersr = fmaxf(size.ersr - 1.0f, 1.0f);
                                glPointSize(size.ersr);
                                glLineWidth(size.ersr);
                                printf("eraser size: %2.0f\n", size.ersr);
                            } else {
                                size.brsh = fmaxf(size.brsh - 1.0f, 1.0f);
                                glPointSize(size.brsh);
                                glLineWidth(size.brsh);
                                printf("brush size: %2.0f\n", size.brsh);
                            } break;
                        case SDLK_t:
                            flag ^= ANTI_ALIAS;
                            if (flag & ANTI_ALIAS) {
                                glEnable(GL_LINE_SMOOTH);
                                glEnable(GL_POLYGON_SMOOTH);
                                printf("Anti-Aliasing ON\n");
                            } else {
                                glDisable(GL_LINE_SMOOTH);
                                glDisable(GL_POLYGON_SMOOTH);
                                printf("Anti-Aliasing OFF\n");
                            } break;
                        default: break;
                    }
                    break;
                case SDL_MOUSEMOTION:
                    if (event.motion.state & SDL_BUTTON_LMASK) {
                        if (ndraws == 0)
                            ndraws++;
                        float x = 2.0f * (event.motion.x / screen.w - 0.5f);
                        float y = -2.0f * (event.motion.y / screen.h - 0.5f);
                        pos[ndraws++] = x + I*y;
                    }
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    if (event.button.button & SDL_BUTTON_LEFT) {
                        float x = 2.0f * (event.motion.x / screen.w - 0.5f);
                        float y = -2.0f * (event.motion.y / screen.h - 0.5f);
                        pos[ndraws++] = x + I*y;
                    }
                    break;
                case SDL_KEYUP:
                    switch (event.key.keysym.sym) {
                        case SDLK_q:
                            delta.r = -keystat[SDL_SCANCODE_A] * DELTA_COR;
                            flag &= keystat[SDL_SCANCODE_A] ? ~0 : ~CHANGE_RED; break;
                        case SDLK_a:
                            delta.r = keystat[SDL_SCANCODE_Q] * DELTA_COR;
                            flag &= keystat[SDL_SCANCODE_Q] ? ~0 : ~CHANGE_RED; break;
                        case SDLK_w:
                            delta.g = -keystat[SDL_SCANCODE_S] * DELTA_COR;
                            flag &= keystat[SDL_SCANCODE_S] ? ~0 : ~CHANGE_GREEN; break;
                        case SDLK_s:
                            delta.g = keystat[SDL_SCANCODE_W] * DELTA_COR;
                            flag &= keystat[SDL_SCANCODE_W] ? ~0 : ~CHANGE_GREEN; break;
                        case SDLK_e:
                            delta.b = -keystat[SDL_SCANCODE_D] * DELTA_COR;
                            flag &= keystat[SDL_SCANCODE_D] ? ~0 : ~CHANGE_BLUE; break;
                        case SDLK_d:
                            delta.b = keystat[SDL_SCANCODE_E] * DELTA_COR;
                            flag &= keystat[SDL_SCANCODE_E] ? ~0 : ~CHANGE_BLUE; break;
                        case SDLK_r:
                            delta.a = keystat[SDL_SCANCODE_F] * DELTA_COR;
                            flag &= keystat[SDL_SCANCODE_F] ? ~0 : ~CHANGE_ALPHA; break;
                        case SDLK_f:
                            delta.a = keystat[SDL_SCANCODE_R] * DELTA_COR;
                            flag &= keystat[SDL_SCANCODE_F] ? ~0 : ~CHANGE_ALPHA; break;
                        default: break;
                    }
                    break;
                default: break;
            }
        }
        if (flag & (CHANGE_RED | CHANGE_GREEN | CHANGE_BLUE | CHANGE_ALPHA) && ~flag & ERASE) {
            cor.r = fminf(fmaxf(cor.r + delta.r, 0.0f), 1.0f);
            cor.g = fminf(fmaxf(cor.g + delta.g, 0.0f), 1.0f);
            cor.b = fminf(fmaxf(cor.b + delta.b, 0.0f), 1.0f);
            cor.a = fminf(fmaxf(cor.a + delta.a, 0.0f), 1.0f);
            glUniform4fv(cor_uni, 1, cor.c);
            printf("r: %.3f   g: %.3f   b: %.3f   a: %.3f\n", cor.r, cor.g, cor.b, cor.a);
        }
        if (ndraws > 0) {
            int repeat = 1;
            glBufferSubData(GL_ARRAY_BUFFER, 0, ndraws*sizeof(float complex), pos);
            do {
                glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, repeat ? fbo : 0);
                (ndraws > 1) ? glDrawArrays(GL_LINE_STRIP, 0, ndraws) : glDrawArrays(GL_POINTS, 0, 1);
            } while (repeat--);
            SDL_GL_SwapWindow(window);
            pos[0] = pos[ndraws-1];
            fps++;
        }
        if (flag & VSYNC)
            SDL_Delay(16 - (SDL_GetTicks() - t));
        if (t - prev_st > 1000) {
            if (flag & FPS)
                printf("fps = %"PRIu32"\n", fps);
            fps = 0;
            prev_st = t;
        }
    }
    // save image drawn
    if (!write_pngfile("hmm.png", fbo))
        fprintf(stderr, "Could not write png file\n");

    jkl(0); 
    // deallocate resources and quit
    glDeleteTextures(1, &tex);
    glDeleteFramebuffersEXT(1, &fbo);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(prog);
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}

GLuint get_shader (GLenum type, const char *path) {
    FILE *file;
    char *buffer, *tmp;
    size_t size = 1024, nread = 0;
    GLuint shader;
    GLint status;

    shader = glCreateShader(type);
    if (!(file = fopen(path, "r"))) jkl(-10);
    if (!(buffer = (char *)aligned_alloc(CACHE_ALIGN, size * sizeof(char)))) jkl(-11);
    if ((nread = fread(buffer, sizeof(char), size, file)) == size) {
        do {
            size *= 2;
            if (!(buffer = (char *)realloc(buffer, size))) jkl(-12);
            tmp = buffer + size/2;
            nread += fread(tmp, sizeof(char), size/2, file);
        } while (nread == size);
    }
    if (fclose(file) == EOF) jkl(-13);
    buffer[nread] = '\0';
    glShaderSource(shader, 1, (const char **)&buffer, NULL);
    glCompileShader(shader);
    glGetShaderInfoLog(shader, size, NULL, buffer);
    fprintf(stderr, "%s", buffer);
    free(buffer);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) jkl(-14);
    return shader;
}

int write_pngfile (const char *filename, GLuint fbo) {
    FILE *file = NULL;
    uchar *buffer = NULL;
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    png_bytepp row_ptrs = NULL;
    size_t y, h, width;

    if (!(file = fopen(filename, "wb")))
        { jkl(40); goto wfile_error; };
    if (!(buffer = (uchar *)malloc(scrnW * scrnH * 4*sizeof(uchar))))
        { jkl(41); goto wbuffer_error; };
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
    glPixelStorei(GL_PACK_ALIGNMENT, 8);
    glReadPixels(0, 0, scrnW, scrnH, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    if (!(png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)))
        { jkl(42); goto wpng_error; };
    if (!(info_ptr = png_create_info_struct(png_ptr)))
        { jkl(43); goto winfo_error; };
    if (setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_write_struct(&png_ptr, &info_ptr);
        free(row_ptrs);
        free(buffer);
        fclose(file);
        return 0;
    }
    png_init_io(png_ptr, file);
    png_set_IHDR(png_ptr, info_ptr, scrnW, scrnH, 8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,\
        PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_ptr, info_ptr);
    if (!(row_ptrs = (png_bytep *)malloc(sizeof(png_bytep) * scrnH)))
        { jkl(45); goto winfo_error; };
    width = png_get_rowbytes(png_ptr, info_ptr);
    for (y = 0, h = scrnH-1; y < scrnH; y++, h--)
        row_ptrs[y] = buffer + h * width;
    png_write_image(png_ptr, row_ptrs);
    png_write_end(png_ptr, NULL);

    png_destroy_write_struct(&png_ptr, &info_ptr);
    free(row_ptrs);
    free(buffer);
    fclose(file);
    return 1;

    winfo_error:
        png_destroy_write_struct(&png_ptr, &info_ptr);
    wpng_error:
        free(buffer);
    wbuffer_error:
        fclose(file);
    wfile_error:
    return 0;
}

int read_pngfile (const char *filename, GLuint fbo) {
    FILE *file = NULL;
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    png_bytepp row_ptrs = NULL;
    png_byte color_type, bit_depth, *buffer = NULL;
    int width, height, y, h, repeat = 1;
    char header[8];

    if (!(file = fopen(filename, "rb")))
        { jkl(46); goto rfile_error; };
    if (!(png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)))
        { jkl(47); goto rpng_error; };
    if (!(info_ptr = png_create_info_struct(png_ptr)))
        { jkl(48); goto rinfo_error; };
    if (setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        free(buffer);
        free(row_ptrs);
        fclose(file);
        return 0;
    }
    png_init_io(png_ptr, file);
    png_read_info(png_ptr, info_ptr);
    color_type = png_get_color_type(png_ptr, info_ptr);
    bit_depth = png_get_bit_depth(png_ptr, info_ptr);
    // transform to usable png
    if (bit_depth == 16)
        png_set_strip_16(png_ptr);
    if (color_type == PNG_COLOR_TYPE_PALETTE)
        png_set_palette_to_rgb(png_ptr);
    else if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
        png_set_expand_gray_1_2_4_to_8(png_ptr);
    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
        png_set_tRNS_to_alpha(png_ptr);
    if (color_type == PNG_COLOR_TYPE_RGB ||
        color_type == PNG_COLOR_TYPE_GRAY ||
        color_type == PNG_COLOR_TYPE_PALETTE)
        png_set_filler(png_ptr, 0xff, PNG_FILLER_AFTER);
    if (color_type == PNG_COLOR_TYPE_GRAY ||
        color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
        png_set_gray_to_rgb(png_ptr);
    // read file
    png_read_update_info(png_ptr, info_ptr);
    width = png_get_rowbytes(png_ptr, info_ptr);
    height = png_get_image_height(png_ptr, info_ptr);
    if (!(row_ptrs = (png_bytep *)malloc(sizeof(png_bytep) * height)))
        { jkl(49); goto rinfo_error;  };
    if (!(buffer = (uchar *)malloc(width * height)))
        { jkl(50); goto rbuffer_error; };
    for (y = 0, h = height-1; y < height; y++, h--)
        row_ptrs[y] = buffer + h * width;
    png_read_image(png_ptr, row_ptrs);
    glPixelStorei(GL_PACK_ALIGNMENT, 8);
    glDisable(GL_BLEND);
    do {
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, repeat ? fbo : 0);
        GL(glDrawPixels(width/4, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer));
    } while (repeat--);
    glEnable(GL_BLEND);
    SDL_GL_SwapWindow(window);
    free(buffer);
    free(row_ptrs);
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    fclose(file);
    return 1;

    rbuffer_error:
        free(row_ptrs);
    rinfo_error:
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    rpng_error:
        fclose(file);
    rfile_error:
    return 0;   
}

void jkl (int i) {
    int errn = errno;
    fprintf(stderr, "Error %d log:\n  SDL2: %s\n  GLEW: %s\n  OpenGL: 0x%04x\n  errno %d: %s\n", i, SDL_GetError(), glewGetErrorString(i), glGetError(), errn, strerror(errn));
    if (i < 0) {
        SDL_GL_DeleteContext(context);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(i);
    }
}

/********DEBUG*ON*OFF*********

(^[^\(=]\s*)(gl[A-Z].*?\));
$1GL($2);

\<GL\((.*)\);
$1;

*****************************/