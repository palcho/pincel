#!/bin/bash
site=cin.ufpe.br/~fcgr
here=$PWD

if [[ $# > 0 ]]; then
    if [[ $1 =~ -c(lean(up)?)?$ ]]; then
        rm -r $HOME/Desktop/SDL2-2.0.5 $HOME/Desktop/glew-2.0.0 $HOME/fcGL
        echo SDL2, GLEW and fcGL deleted
        exit 0
    fi
fi

cd $HOME/fcGL || (
    echo installing SDL2 and GLEW
    mkdir $HOME/fcGL

    # SDL2
    cd $HOME/Desktop
    wget https://www.libsdl.org/release/SDL2-2.0.5.zip || wget $site/SDL2-2.0.5.zip
    unzip SDL2-2.0.5.zip
    rm SDL2-2.0.5.zip
    cd SDL2-2.0.5
    ./configure --prefix=$HOME/fcGL
    make
    make install
    export LD_LIBRARY_PATH=$HOME/fcGL/lib

    # glew
    cd $HOME/Desktop
    wget https://ufpr.dl.sourceforge.net/project/glew/glew/2.0.0/glew-2.0.0.zip || wget $site/glew-2.0.0.zip
    unzip glew-2.0.0.zip
    rm glew-2.0.0.zip
    cd glew-2.0.0
    make
    cp -r include/GL $HOME/fcGL/include
    cd lib
    cp * $HOME/fcGL/lib

    # libpng
    #cd $HOME/Desktop
    #wget https://ufpr.dl.sourceforge.net/project/libpng/libpng16/1.6.32/libpng-1.6.32.tar.xz
    # etc: untar, install and move files to fcGL
    # but fist: check if libpng isn't already installed with:
    # dpkg --list | grep png
)

# compile
echo compiling and running
cd $here
gcc -std=c11 pincel.c -I$HOME/fcGL/include -L$HOME/fcGL/lib -L/usr/lib/x86_64-linux-gnu/mesa -O3 -lGLEW -lSDL2 -lGL -lm -lpng -o pincel
export LD_LIBRARY_PATH=$HOME/fcGL/lib:/usr/lib/x86_64-linux-gnu/mesa
./pincel $1 $2 $3