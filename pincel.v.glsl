#version 120

attribute vec2 pos;

uniform vec4 cor;

varying vec4 color;

void main (void) {
    color = cor;
    gl_Position = vec4(pos, 0.0, 1.0);
    //gl_PointSize = 2.0;
}